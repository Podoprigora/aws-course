Week 4 – Networking: VPC, ELB

Tasks (all flow is explained in corresponding presentation aws-for-devs-2021-week04.pptx):

Create a Terraform script which will create the next infrastructure:
Virtual Public Cloud (VPC) with two subnets private and public, each with one EC2 instance with Apache Web server installed and with simple index.html
NAT EC2 instance which will give HTTP access for the private subnet. ATTENTION. AWS suggests to use NAT GATEWAY, but it costs. 
Application Load Balancer targeting to both private and public subnets

Use load balancer HTTP URL to check your solution - load balancer should return as a response these pages per extern HTTP-request. So you have to see pages from both servers.

Services to learn:

VPC
NAT EC2 instances
ELB

Week 4 implementation path:

Create all needful infrastructure via AWS Management console (UI) – you could use the mentioned presentation as example
Create a Terraform script which will generate all needful infrastructure automatically

Week 4 output:

A Terraform script which will generate all described infrastructure
When the stack is up and running – copy load balancer URL from the script output and paste into the browser (or curl command) – you should see pages from both servers randomly 
