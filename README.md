# AWS as a platform for Devs.
## Course by Lohika.
Start 5/03/21.

## Week 0.

```
aws cloudformation create-stack \
--stack-name w0stack \
--template-body file:///<path to w0template.json> \
--region=us-west-2 \
--parameters ParameterKey=InstanceTypeParameter,ParameterValue=<myInstanceType>
```

```
ssh -i "first-home-mac.pem" ec2-user@ec2-34-221-28-25.us-west-2.compute.amazonaws.com
```

```
aws cloudformation delete-stack \
--stack-name w0stack
```

```
aws cloudformation create-stack \
--stack-name w1stack \
--template-body file:///<path to w1template.json> \
--region us-west-2 \
--parameters ParameterKey=InstanceTypeParameter,ParameterValue=t2.micro
```

## Week 5.

```
aws sns publish \
--topic-arn "arn:aws:sns:us-west-2:438130883600:w5_sns_topic" \
--message "Hello from SNS! " \
--region=us-west-2
```
```
aws sqs send-message \
--queue-url "https://sqs.us-west-2.amazonaws.com/438130883600/w5_sqs_queue"\
--message-body "New message from EC2" \
--region=us-west-2
```