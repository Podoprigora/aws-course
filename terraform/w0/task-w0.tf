terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_instance" "w0-ec2" {
  ami           = "ami-00f9f4069d04c0c6e"
  instance_type = var.instance_type
  user_data     = filebase64("init-script.sh")
  key_name      = "first-home-mac"
  security_groups = [
    aws_security_group.sg-w0-ec2.name
  ]

  tags = {
    Name = "W0-Ec2Instance"
  }
}

resource "aws_security_group" "sg-w0-ec2" {
  name = "w0-ec2-sg"
  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
    "0.0.0.0/0"]
  }
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.w0-ec2.public_ip
}
