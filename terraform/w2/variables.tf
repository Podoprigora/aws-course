variable "instance_type" {
  default = "t2.micro"
}
variable "region" {
  default = "us-west-2"
}