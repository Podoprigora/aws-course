terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region = "us-west-2"
}

resource "aws_instance" "w2-ec2" {
  ami = "ami-00f9f4069d04c0c6e"
  instance_type = var.instance_type
  user_data = filebase64("init-script.sh")
  iam_instance_profile = aws_iam_instance_profile.w2_instance_profile.id

  security_groups = [
    aws_security_group.sg-w2.name
  ]

  tags = {
    Name = "W2-Ec2Instance"
  }
}

resource "aws_iam_instance_profile" "w2_instance_profile" {
  name = "w2_instance_profile"
  role = aws_iam_role.w2_iam_role.name
}

resource "aws_iam_role" "w2_iam_role" {
  name = "w2_iam_role"
  description = "ec2 assume role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "s3-access-policy" {
  name        = "s3-access-policy"
  description = "My policy for s3 access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect: "Allow"
        Action: "s3:*"
        Resource: "*"
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "w2-roles-to-policy-attach" {
  name       = "roles-to-policy-attach"
  roles      = [aws_iam_role.w2_iam_role.name]
  policy_arn = aws_iam_policy.s3-access-policy.arn
}

resource "aws_security_group" "sg-w2" {
  name = "w2-sg"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}
