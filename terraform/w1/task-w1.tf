terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region = var.region
}

resource "aws_autoscaling_group" "w1-asg" {
  name = "w1-asg"
  availability_zones = [
    "us-west-2b"]
  max_size = 2
  min_size = 2
  desired_capacity = 2

  launch_template {
    id = aws_launch_template.w1-launch_template.id
    version = "$Latest"
  }
}

resource "aws_launch_template" "w1-launch_template" {
  name = "w1-launch_template"
  image_id = "ami-00f9f4069d04c0c6e"
  vpc_security_group_ids = [
    aws_security_group.sg-w1-asg.id]
  instance_type = var.instance_type
  user_data = filebase64("init-script.sh")
}

resource "aws_security_group" "sg-w1-asg" {
  name = "w1-asg-sg"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = [
      "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port = 0
    protocol = "-1"
    cidr_blocks = [
      "0.0.0.0/0"]
  }
}