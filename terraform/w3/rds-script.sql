CREATE SCHEMA IF NOT EXISTS test;

CREATE TABLE test.awesome_lectures
(
    id      integer      NOT NULL,
    content varchar(256) NOT NULL
);

INSERT INTO test.awesome_lectures
VALUES (666, 'RTFM!');

SELECT *
FROM test.awesome_lectures;