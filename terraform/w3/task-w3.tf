terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

resource "aws_db_instance" "w3-rds" {
  allocated_storage      = 10
  engine                 = "postgres"
  engine_version         = "12.5"
  instance_class         = "db.t2.micro"
  name                   = "mydb"
  username               = "postgres"
  password               = "password"
  publicly_accessible    = true
  skip_final_snapshot    = true
  vpc_security_group_ids = [aws_security_group.sg-w3.id]
}

resource "aws_instance" "w3-ec2" {
  ami                  = "ami-00f9f4069d04c0c6e"
  instance_type        = var.instance_type
  user_data            = filebase64("init-script.sh")
  key_name             = "first-home-mac"
  iam_instance_profile = aws_iam_instance_profile.w3_instance_profile.id

  security_groups = [
    aws_security_group.sg-w3.name
  ]

  tags = {
    Name = "w3-Ec2Instance"
  }
}

resource "aws_dynamodb_table" "w3-dynamodb-table" {
  name           = "MusicCollection"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "Artist"
  range_key      = "SongTitle"

  attribute {
    name = "Artist"
    type = "S"
  }

  attribute {
    name = "SongTitle"
    type = "S"
  }
}

resource "aws_iam_instance_profile" "w3_instance_profile" {
  name = "w3_instance_profile"
  role = aws_iam_role.w3_iam_role.name
}

resource "aws_iam_role" "w3_iam_role" {
  name        = "w3_iam_role"
  description = "ec2 assume role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

resource "aws_iam_policy" "resources-access-policy" {
  name        = "resources-access-policy"
  description = "My policy for s3 access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect : "Allow"
        Action : "s3:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "rds-db:connect"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "dynamodb:*"
        Resource : "*"
      }
    ]
  })
}

resource "aws_iam_policy_attachment" "w3-roles-to-policy-attach" {
  name       = "roles-to-policy-attach"
  roles      = [aws_iam_role.w3_iam_role.name]
  policy_arn = aws_iam_policy.resources-access-policy.arn
}

resource "aws_security_group" "sg-w3" {
  name = "w3-sg"
  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  ingress {
    from_port = 5432
    to_port   = 5432
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  ingress {
    from_port = 22
    to_port   = 22
    protocol  = "tcp"
    cidr_blocks = [
    "0.0.0.0/0"]
  }

  egress {
    from_port = 0
    to_port   = 0
    protocol  = "-1"
    cidr_blocks = [
    "0.0.0.0/0"]
  }
}

output "instance_public_endpoint" {
  description = "RDS endpoint"
  value       = aws_db_instance.w3-rds.endpoint
}

output "instance_port" {
  description = "RDS port"
  value       = aws_db_instance.w3-rds.port
}
