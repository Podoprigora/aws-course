#!/bin/bash
aws dynamodb list-tables --region us-west-2 
# aws dynamodb create-table \
#     --table-name MusicCollection \
#     --attribute-definitions AttributeName=Artist,AttributeType=S AttributeName=SongTitle,AttributeType=S \
#     --key-schema AttributeName=Artist,KeyType=HASH AttributeName=SongTitle,KeyType=RANGE  --region us-west-2 
aws dynamodb put-item \
    --table-name MusicCollection \
    --item file://item.json --region us-west-2 
aws dynamodb query \
    --table-name MusicCollection --region us-west-2 \
    --key-condition-expression "Artist = :name" \
    --expression-attribute-values  '{":name":{"S":"No One You Know"}}'