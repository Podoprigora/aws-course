#!/bin/bash
aws s3api create-bucket --bucket w3-andrulez-27-03-21 --region us-west-2 --create-bucket-configuration LocationConstraint=us-west-2
aws s3 cp dynamodb-script.sh s3://w3-andrulez-27-03-21/dynamodb-script.sh
aws s3 cp item.json s3://w3-andrulez-27-03-21/item.json
aws s3 cp rds-script.sql s3://w3-andrulez-27-03-21/rds-script.sql
