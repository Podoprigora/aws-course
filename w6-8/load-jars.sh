#!/bin/bash
aws s3 mb s3://final-task-jars --region us-west-2
aws s3api put-bucket-versioning --bucket final-task-jars --versioning-configuration Status=Enabled
aws s3 cp ./jars/calc-2021-0.0.2-SNAPSHOT.jar s3://final-task-jars
aws s3 cp ./jars/persist3-2021-0.0.1-SNAPSHOT.jar s3://final-task-jars
