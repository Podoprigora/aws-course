#!/bin/bash
sudo yum -y update
sudo yum install -y java-1.8.0-openjdk-devel
aws s3 cp s3://final-task-jars/persist3-2021-0.0.1-SNAPSHOT.jar persist.jar
export RDS_HOST=${aws_db_instance.rds.address}
sudo java -jar persist.jar