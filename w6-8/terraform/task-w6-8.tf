terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# vpc
resource "aws_vpc" "vpc" {
  cidr_block       = "192.168.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "my-vpc"
  }

  enable_dns_hostnames = true
}

# internet gateway
resource "aws_internet_gateway" "internet_gateway" {
  depends_on = [
    aws_vpc.vpc,
  ]

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "my-internet-gateway"
  }
}

# route table with target as internet gateway
resource "aws_route_table" "IG_route_table" {
  depends_on = [
    aws_vpc.vpc,
    aws_internet_gateway.internet_gateway,
  ]

  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.internet_gateway.id
  }

  tags = {
    Name = "IG-route-table"
  }
}

# public subnet 1
resource "aws_subnet" "public_subnet_1" {
  depends_on = [
    aws_vpc.vpc,
  ]

  vpc_id     = aws_vpc.vpc.id
  cidr_block = "192.168.0.0/24"

  availability_zone_id = "usw2-az1"

  tags = {
    Name = "public-subnet-1"
  }

  map_public_ip_on_launch = true
}

# associate route table to public subnet 1
resource "aws_route_table_association" "associate_routetable_to_public_subnet_1" {
  depends_on = [
    aws_subnet.public_subnet_1,
    aws_route_table.IG_route_table,
  ]
  subnet_id      = aws_subnet.public_subnet_1.id
  route_table_id = aws_route_table.IG_route_table.id
}

# public subnet 2
resource "aws_subnet" "public_subnet_2" {
  depends_on = [
    aws_vpc.vpc,
  ]

  vpc_id     = aws_vpc.vpc.id
  cidr_block = "192.168.2.0/24"

  availability_zone_id = "usw2-az2"

  tags = {
    Name = "public-subnet-2"
  }

  map_public_ip_on_launch = true
}

# associate route table to public subnet 2
resource "aws_route_table_association" "associate_routetable_to_public_subnet_2" {
  depends_on = [
    aws_subnet.public_subnet_2,
    aws_route_table.IG_route_table,
  ]
  subnet_id      = aws_subnet.public_subnet_2.id
  route_table_id = aws_route_table.IG_route_table.id
}

# route table with target as NAT
resource "aws_route_table" "private_subnet_1_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat.id
  }

  tags = {
    Name = "Private Subnet 1"
  }
}

# private subnet 1
resource "aws_subnet" "private_subnet_1" {
  depends_on = [
    aws_vpc.vpc,
  ]

  vpc_id     = aws_vpc.vpc.id
  cidr_block = "192.168.1.0/24"

  availability_zone_id = "usw2-az2"

  tags = {
    Name = "private-subnet-1"
  }
}

# associate route table to private subnet 1
resource "aws_route_table_association" "private_subnet_1_association" {
  subnet_id      = aws_subnet.private_subnet_1.id
  route_table_id = aws_route_table.private_subnet_1_route_table.id
}

# private subnet 2
resource "aws_subnet" "private_subnet_2" {
  depends_on = [
    aws_vpc.vpc,
  ]

  vpc_id     = aws_vpc.vpc.id
  cidr_block = "192.168.3.0/24"

  availability_zone_id = "usw2-az1"

  tags = {
    Name = "private-subnet-2"
  }
}

# route table with target as NAT
resource "aws_route_table" "private_subnet_2_route_table" {
  vpc_id = aws_vpc.vpc.id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat.id
  }

  tags = {
    Name = "Private Subnet 2"
  }
}

# associate route table to private subnet 1
resource "aws_route_table_association" "private_subnet_2_association" {
  subnet_id      = aws_subnet.private_subnet_2.id
  route_table_id = aws_route_table.private_subnet_2_route_table.id
}

/*
  NAT Instance
*/
resource "aws_security_group" "nat" {
  name        = "vpc_nat"
  description = "Allow traffic to pass from the private subnet to the internet"

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private_subnet_1.cidr_block]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = [aws_subnet.private_subnet_1.cidr_block]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [aws_vpc.vpc.cidr_block]
  }
  egress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "NATSG"
  }
}

resource "aws_instance" "nat" {
  ami                         = "ami-0032ea5ae08aa27a2" #aws ec2 describe-images --filter Name="name",Values="amzn-ami-vpc-nat*"
  availability_zone           = aws_subnet.public_subnet_1.availability_zone
  instance_type               = "t2.micro"
  key_name                    = "first-home-mac"
  vpc_security_group_ids      = [aws_security_group.nat.id]
  subnet_id                   = aws_subnet.public_subnet_1.id
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = "VPC NAT"
  }
}

resource "aws_default_route_table" "default_route_table" {
  default_route_table_id = aws_vpc.vpc.default_route_table_id

  route {
    cidr_block  = "0.0.0.0/0"
    instance_id = aws_instance.nat.id
  }

  tags = {
    Name = "default (private) route table targeting to nat ec2 instance"
  }
}

#  ec2 instance profile with IAM role
resource "aws_iam_instance_profile" "ec2_instance_profile" {
  name = "ec2_instance_profile"
  role = aws_iam_role.ec2_iam_role.name
}
# IAM assume role for EC2
resource "aws_iam_role" "ec2_iam_role" {
  name        = "ec2_iam_role"
  description = "ec2 assume role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}
# Attach access policy to assume ec2 role
resource "aws_iam_policy_attachment" "roles-to-policy-attach" {
  name       = "roles-to-policy-attach"
  roles      = [aws_iam_role.ec2_iam_role.name]
  policy_arn = aws_iam_policy.resources-access-policy.arn
}

#  IAM policy to access to S3, SNS, SQS, RDS, DynamoDB 
resource "aws_iam_policy" "resources-access-policy" {
  name        = "resources-access-policy"
  description = "My policy for resources access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect : "Allow"
        Action : "s3:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sqs:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sns:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "rds-db:connect"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "dynamodb:*"
        Resource : "*"
      }
    ]
  })
}
#  Security group to allow SSH/HTTP/HTTPS connection to ec2 instance
resource "aws_security_group" "ec2_sg" {
  name        = "ec2_sg"
  description = "Allow incoming SSH/HTTP connections."

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "ec2_sg"
  }
}


#  Security group to allow Sconnection to rds instance
resource "aws_security_group" "rds_sg" {
  name        = "rds_sg"
  description = "Allow rds connections."

  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = -1
    to_port     = -1
    protocol    = "icmp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  vpc_id = aws_vpc.vpc.id

  tags = {
    Name = "rds_sg"
  }
}

# --------EC2-----------------

resource "aws_autoscaling_group" "asg" {
  name             = "asg"
  max_size         = 2
  min_size         = 2
  desired_capacity = 2
  # Conflicts with availability_zones
  vpc_zone_identifier = [aws_subnet.public_subnet_1.id, aws_subnet.public_subnet_2.id]

  launch_template {
    id      = aws_launch_template.asg-launch-template.id
    version = "$Latest"
  }
}

resource "aws_launch_template" "asg-launch-template" {
  name     = "asg-launch-template"
  image_id = "ami-00f9f4069d04c0c6e"
  vpc_security_group_ids = [
  aws_security_group.ec2_sg.id]
  instance_type = var.instance_type
  user_data     = filebase64("init-script-public.sh")
  iam_instance_profile  {
    name = aws_iam_instance_profile.ec2_instance_profile.id
  }
}

# Private subnets - only one ec2 in only one subnet
# private subnet 1 ec2 instance
resource "aws_instance" "ec2-priv-1" {
  ami                         = "ami-00f9f4069d04c0c6e"
  availability_zone           = aws_subnet.private_subnet_1.availability_zone
  instance_type               = var.instance_type
  user_data                   = filebase64("init-script-private.sh")
  key_name                    = "first-home-mac"
  iam_instance_profile        = aws_iam_instance_profile.ec2_instance_profile.id
  vpc_security_group_ids      = [aws_security_group.rds_sg.id]
  subnet_id                   = aws_subnet.private_subnet_1.id
  associate_public_ip_address = true
  source_dest_check           = false

  tags = {
    Name = "Instance inside private subnet 1"
  }
}

# ------------------END-EC2-------------

# Load balanser ALB
resource "aws_lb" "alb" {
  name               = "alb"
  internal           = false
  load_balancer_type = "application"

  security_groups = [aws_security_group.ec2_sg.id]
  subnets = [
    aws_subnet.public_subnet_1.id,
    aws_subnet.public_subnet_2.id

  ]

  enable_deletion_protection = false

  tags = {
    Name = "ALB"
  }
}

resource "aws_alb_target_group" "alb_target_group" {
  name     = "alb-target-group"
  port     = 80
  protocol = "HTTP"
  vpc_id   = aws_vpc.vpc.id
  health_check {
    path                = "/health"
    port                = 80
    healthy_threshold   = 5
    unhealthy_threshold = 2
    timeout             = 2
    interval            = 10
    matcher             = "200" # has to be HTTP 200 or fails
  }
}

resource "aws_alb_listener" "lb_listener" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_alb_target_group.alb_target_group.arn
  }
}

resource "aws_autoscaling_attachment" "asg_attachment" {
  autoscaling_group_name = aws_autoscaling_group.asg.id
  alb_target_group_arn   = aws_alb_target_group.alb_target_group.arn
}

#  SNS topic resource
resource "aws_sns_topic" "edu-lohika-training-aws-sns-topic" {
  name = "edu-lohika-training-aws-sns-topic"

  tags = {
    Name = "edu-lohika-training-aws-sns-topic"
  }
}

#  SQS queue resource
resource "aws_sqs_queue" "edu-lohika-training-aws-sqs-queue" {
  name                      = "edu-lohika-training-aws-sqs-queue"
  delay_seconds             = 1
  max_message_size          = 262144
  message_retention_seconds = 600
  receive_wait_time_seconds = 1

  tags = {
    Name = "edu-lohika-training-aws-sqs-queue"
  }
}

resource "aws_db_instance" "rds" {
  allocated_storage      = 10
  engine                 = "postgres"
  engine_version         = "12.5"
  instance_class         = "db.t2.micro"
  name                   = "EduLohikaTrainingAwsRds"
  username               = "rootuser"
  password               = "rootuser"
  publicly_accessible    = true
  skip_final_snapshot    = true
  db_subnet_group_name   = aws_db_subnet_group.rds_subnet_group.name
  vpc_security_group_ids = [aws_security_group.rds_sg.id]
}

resource "aws_db_subnet_group" "rds_subnet_group" {
  name       = "main"
  subnet_ids = [aws_subnet.private_subnet_1.id, aws_subnet.private_subnet_2.id]

  tags = {
    Name = "My DB subnet group"
  }

}

resource "aws_dynamodb_table" "dynamodb-table" {
  name           = "edu-lohika-training-aws-dynamodb"
  billing_mode   = "PROVISIONED"
  read_capacity  = 2
  write_capacity = 2
  hash_key       = "UserName"


  attribute {
    name = "UserName"
    type = "S"
  }
}

# -----------Outputs--------------
output "dns_alb" {
  description = "dns_name of ALB resouce"
  value       = aws_lb.alb.dns_name
}

output "sns_topic_arn" {
  description = "SNS topic ARN"
  value       = aws_sns_topic.edu-lohika-training-aws-sns-topic.arn
}

output "sqs_queu_url" {
  description = "SQS queu URL"
  value       = aws_sqs_queue.edu-lohika-training-aws-sqs-queue.id
}

# Just for debug
output "instance_public_endpoint" {
  description = "RDS endpoint"
  value       = aws_db_instance.rds.endpoint
}