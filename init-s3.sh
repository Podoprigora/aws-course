#!/bin/bash
echo "Week 2 goes well." > sample.txt
aws s3 mb s3://w2-andrulez-20-03-21 --region us-west-2
aws s3api put-bucket-versioning --bucket w2-andrulez-20-03-21 --versioning-configuration Status=Enabled
aws s3 cp sample.txt s3://w2-andrulez-20-03-21

# Experiments
#aws s3 rm s3://w2-andrulez-20-03-21/sample.txt --recursive
#aws s3 cp sample.txt s3://w2-andrulez-20-03-21 --acl public-read
#aws s3 cp sample.txt s3://w2-andrulez-20-03-21 --acl public-read-write
#aws s3 cp s3://w2-andrulez-20-03-21/sample.txt ./from-s3-sample.txt
