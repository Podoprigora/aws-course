terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.27"
    }
  }
}

provider "aws" {
  profile = "default"
  region  = var.region
}

# EC2 instance
resource "aws_instance" "w5-ec2" {
  ami                  = "ami-00f9f4069d04c0c6e"
  instance_type        = var.instance_type
  key_name             = "first-home-mac"
  iam_instance_profile = aws_iam_instance_profile.w5_instance_profile.id

  security_groups = [
    aws_security_group.sg-w5.name
  ]

  tags = {
    Name = "w5-Ec2Instance"
  }
}

#  EC2 instance profile with IAM role
resource "aws_iam_instance_profile" "w5_instance_profile" {
  name = "w5_instance_profile"
  role = aws_iam_role.w5_iam_role.name
}

#  IAM assume role for EC2
resource "aws_iam_role" "w5_iam_role" {
  name        = "w5_iam_role"
  description = "ec2 assume role"
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "ec2.amazonaws.com"
        }
      }
    ]
  })
}

#  IAM policy to access to S3, SNS, SQS 
resource "aws_iam_policy" "resources-access-policy" {
  name        = "resources-access-policy"
  description = "My policy for resources access"

  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Effect : "Allow"
        Action : "s3:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sqs:*"
        Resource : "*"
      },
      {
        Effect : "Allow"
        Action : "sns:*"
        Resource : "*"
      }
    ]
  })
}

# Attach access policy to assume EC2 role
resource "aws_iam_policy_attachment" "w5-roles-to-policy-attach" {
  name       = "roles-to-policy-attach"
  roles      = [aws_iam_role.w5_iam_role.name]
  policy_arn = aws_iam_policy.resources-access-policy.arn
}

#  Security group to allow SSH/HTTP/HTTPS connection to EC2 instance
resource "aws_security_group" "sg-w5" {
  name = "w5-sg"
  ingress {
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

#  SNS topic resource
resource "aws_sns_topic" "w5_sns_topic" {
  name = "w5_sns_topic"

  tags = {
    Name = "w5_sns_topic"
  }
}

#  SQS queue resource
resource "aws_sqs_queue" "w5_sqs_queue" {
  name                      = "w5_sqs_queue"
  delay_seconds             = 1
  max_message_size          = 262144
  message_retention_seconds = 600
  receive_wait_time_seconds = 1

  tags = {
    Name = "w5_sqs_queue"
  }
}

output "instance_public_ip" {
  description = "Public IP address of the EC2 instance"
  value       = aws_instance.w5-ec2.public_ip
}

output "sns_topic_arn" {
  description = "SNS topic ARN"
  value       = aws_sns_topic.w5_sns_topic.arn
}

output "sqs_queu_url" {
  description = "SQS queu URL"
  value       = aws_sqs_queue.w5_sqs_queue.id
}